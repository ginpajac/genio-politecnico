/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.view;

import espol.edu.ec.model.JuegoModel;
import espol.edu.ec.model.GuardarPregunta;
import espol.edu.ec.tdas.ABD;
import espol.edu.ec.tdas.Node;
import espol.edu.ec.model.CreaArbol;
import java.util.Stack;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javax.swing.JOptionPane;

/**
 *
 * @author Grupo8
 * @version
 */
public class Juego {

    private final BorderPane pane;
    private Button buttonYes;
    private Button buttonNo;
    private Label label;
    private final JuegoModel game;
    private final ABD arbol;
    private final Node<String> raiz;
    private final CreaArbol fun;

    public Juego() {
        pane = new BorderPane();
        arbol = new ABD();
        fun = new CreaArbol();
        Stack<Node<String>> datos = fun.crearPila("datos.txt");
        raiz = arbol.cargarArbol(datos);
        game = new JuegoModel(this.raiz);
        pane.setCenter(generarVBox());
        jugar(this.raiz);

    }

    public BorderPane getroot() {
        return pane;
    }

    private VBox generarVBox() {
        VBox vbox = new VBox();
        label = new Label(game.getMessage());
        vbox.setPadding(new Insets(30, 100, 30, 80));
        vbox.setSpacing(20);
        Text titulo = new Text("Genio Politécnico");
        titulo.setId("title-text");
        vbox.getChildren().addAll(titulo, label, generabotones());
        vbox.setAlignment(Pos.CENTER);
        return vbox;
    }

    private HBox generabotones() {
        HBox hbox = new HBox();
        buttonNo = new Button("No!");
        buttonYes = new Button("Si!");
        hbox.getChildren().addAll(buttonYes, buttonNo);
        hbox.setSpacing(20);
        hbox.setAlignment(Pos.CENTER);
        return hbox;
    }

    private void jugar(Node<String> root) {
        game.setMessage(root.getData().substring(3, root.getData().length()));
        label.setText(game.getMessage());
        buttonYes.setOnAction(a -> {
            if (root.getRight() != null) {
                jugar(root.getLeft());
            } else {
                if (root.getData().contains("#R")) {
                    game.setMessage("he adivinado ^^");
                    jugarDeNuevo();
                }
                label.setText(game.getMessage());
            }
        });
        buttonNo.setOnAction(a -> {
            if (root.getLeft() != null) {
                jugar(root.getRight());
            } else {
                game.setMessage("He perdido!");
                String name = JOptionPane.showInputDialog(null, "Ayudame a mejorar\n..en que animal pensabas?", "Animal", JOptionPane.INFORMATION_MESSAGE);
                if (name == null || name.equals("")) {
                    System.exit(0);
                }
                String correctAnimal = name;
                String incorrecto = root.getData().substring(3, root.getData().length());
                String pregunta = JOptionPane.showInputDialog(null, "Una pregunta que diferencie un " + correctAnimal + " de un " + incorrecto, "Question", JOptionPane.INFORMATION_MESSAGE);
                if (pregunta == null || pregunta.equals("")) {
                    System.exit(0);
                }
                GuardarPregunta g = new GuardarPregunta(this.raiz);
                g.guardarArbol(root, correctAnimal, pregunta, this.fun);
                jugarDeNuevo();
            }
            label.setText(game.getMessage());
        });

    }

    private void jugarDeNuevo() {
        game.setMessage(game.getMessage() + "\nDeseas jugar de nuevo:");
        label.setText(game.getMessage());
        buttonYes.setOnAction(a -> jugar(this.raiz));
        buttonNo.setOnAction(a -> buttonNo.setOnMouseClicked(e -> Platform.exit()));
    }

}
