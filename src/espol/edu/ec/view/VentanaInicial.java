/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 *
 * @author Grupo8
 * @version
 */
public class VentanaInicial {

    private final VBox pane;
    private Button btn;

    public Button getBtn() {
        return btn;
    }

    public VentanaInicial() {
        pane = generarVBox();
    }

    public VBox getroot() {
        return pane;
    }

    private VBox generarVBox() {
        btn = new Button(" Play!!  ");
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(50, 120, 110, 120));
        vbox.setSpacing(20);
        Label label = new Label("\nBienvenido a Genio Politécnico!\n"
                + "Primero..Piense en un animal\ny yo trataré de adivinarlo…\n"
                + "Muy bien!");
        Text titulo = new Text("Genio Politécnico");
        titulo.setId("title-text");
        vbox.getChildren().addAll(titulo, label, btn);
        vbox.setAlignment(Pos.CENTER);
        return vbox;
    }

}
