/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.model;

import espol.edu.ec.tdas.Node;

/**
 *
 * @author Grupo8
 */
public class JuegoModel {

    private String message = "";

    public void setMessage(String message) {
        this.message = message;
    }

    public JuegoModel(Node<String> principal) {
        this.message = principal.getData().substring(3, principal.getData().length());
    }

    public String getMessage() {
        return message;
    }

}
