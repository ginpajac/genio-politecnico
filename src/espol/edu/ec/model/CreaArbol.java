package espol.edu.ec.model;

import espol.edu.ec.tdas.Node;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Grupo8
 */
public class CreaArbol {

    public Stack<Node<String>> crearPila(String archivo) {
        Stack<Node<String>> pilaDatos = new Stack();
        try (Scanner sc = new Scanner(new File(archivo))) {
            while (sc.hasNextLine()) {
                Node<String> nodo = new Node(sc.nextLine());
                pilaDatos.push(nodo);
            }
        } catch (FileNotFoundException ex) {
            ex.toString();
        }
        return voltearPila(pilaDatos);
    }

    private Stack<Node<String>> voltearPila(Stack<Node<String>> pila) {
        Stack<Node<String>> nueva = new Stack();
        while (!pila.empty()) {
            nueva.push(pila.pop());
        }
        return nueva;
    }

    public void borrarTxT(String nombre) {
        try {
            File f = new File(nombre);
            FileWriter escritor = new FileWriter(f);
            try (BufferedWriter bW = new BufferedWriter(escritor); PrintWriter salida = new PrintWriter(bW)) {
                salida.write("");
            }
        } catch (IOException e) {
            e.toString();
        }
    }

}
