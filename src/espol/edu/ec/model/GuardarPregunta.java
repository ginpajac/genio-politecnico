/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.model;

import espol.edu.ec.tdas.Node;
import java.awt.Component;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author Grupo8
 */
public class GuardarPregunta {

    private String correcto;
    private String pregunta;
    private final Node<String> raiz;

    public GuardarPregunta(Node<String> raiz) {
        this.raiz = raiz;
    }

    public void guardarArbol(Node<String> ultimoNodo, String correctoAnimal, String preguntaAnimal, CreaArbol ayu) {
        Node<String> opcDer = new Node(ultimoNodo.getData());
        String respuesta;
        while (true) {
            if (pregunta("Para un " + correctoAnimal + " , la respuesta a la pregunta: " + preguntaAnimal + " , es si o no?")) {
                respuesta = "si";
                this.pregunta = preguntaAnimal;
                this.correcto = correctoAnimal;
                break;
            } else {
                respuesta = "no";
                this.pregunta = preguntaAnimal;
                this.correcto = correctoAnimal;
                break;
            }
        }
        Node<String> opcIzq = new Node("#R " + correcto);
        ultimoNodo.setData("#P " + pregunta);
        if ("si".equals(respuesta)) {
            ultimoNodo.setRight(opcDer);
            ultimoNodo.setLeft(opcIzq);
        } else {
            ultimoNodo.setRight(opcIzq);
            ultimoNodo.setLeft(opcDer);
        }
        ayu.borrarTxT("datos.txt");
        arbolArchivo("datos.txt", this.raiz);
    }

    public static boolean pregunta(String texto) {
        String answer = "Q";
        Component frame = null;
        int n = JOptionPane.showConfirmDialog(frame, texto, "Por ultimo...", JOptionPane.YES_NO_OPTION);
        if (n == 0) {
            answer = "Y";
        }
        if (n == 1) {
            answer = "N";
        }
        return answer.startsWith("Y");
    }

    public void arbolArchivo(String archivo, Node<String> n) {
        if (n == null) {
            return;
        }
        crearTxT(archivo, n.getData());
        arbolArchivo(archivo, n.getLeft());
        arbolArchivo(archivo, n.getRight());
    }

    private void crearTxT(String archivo, String contenido) {
        try {
            File f = new File(archivo);
            FileWriter escritor = new FileWriter(f, true);
            try (BufferedWriter bW = new BufferedWriter(escritor); PrintWriter salida = new PrintWriter(bW)) {
                salida.write(contenido + "\r\n");
            }
        } catch (IOException e) {
            e.toString();
        }
    }

}
