package espol.edu.ec.main;

import espol.edu.ec.view.VentanaInicial;
import espol.edu.ec.view.Juego;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Grupo8
 */
public class Game extends Application {

    @Override
    public void start(Stage primaryStage) throws FileNotFoundException {
        Stage ventana;
        Scene sceneInicial;
        Scene sceneJuego;
        ventana = primaryStage;

        Juego game = new Juego();
        VentanaInicial principal = new VentanaInicial();
        sceneInicial = new Scene(principal.getroot(),610,420);
        sceneJuego = new Scene(game.getroot(), 580, 350);
        sceneInicial.getStylesheets().add(getClass().getResource("miEstilo.css").toExternalForm());
        sceneJuego.getStylesheets().add(getClass().getResource("miEstilo.css").toExternalForm());
        ventana.getIcons().add(new Image("imagen/ima.jpg"));
        ventana.setResizable(false);
        principal.getBtn().setOnAction(e
                -> ventana.setScene(sceneJuego)
        );
        //Ventana
        ventana.setTitle("GenioPolitecnico");
        ventana.setScene(sceneInicial);
        ventana.show();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);

    }

}
