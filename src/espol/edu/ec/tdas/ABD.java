/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.tdas;

import java.util.Stack;

/**
 *
 * @author Grupo8
 */
public class ABD<E> {

    private Node<String> raiz;

    public ABD() {
        this.raiz = null;
    }

    public boolean isEmpty() {
        return this.raiz == null;
    }

    public Node<String> getRaiz() {
        return raiz;
    }

    public void setRaiz(Node<String> raiz) {
        this.raiz = raiz;
    }

    public Node<String> cargarArbol(Stack<Node<String>> datos) {
        Node<String> nuevo = new Node<>(datos.pop().getData());

        if ((nuevo.getData()).contains("#P")) {
            nuevo.setLeft(cargarArbol(datos));
            nuevo.setRight(cargarArbol(datos));

        }
        if ((nuevo.getData()).contains("#R")) {
            return nuevo;
        }
        return nuevo;
    }

}
