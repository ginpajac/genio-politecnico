# Genio Polit�cnico

El juego es una simulaci�n del popular juego akinator. El juego utiliza un �rbol de
decisi�n como estructura de datos para representar preguntas y respuestas. Su simulaci�n se
llamar� Genio Polit�cnico y ser� experto en el conocimiento de Animales, por lo tanto, usted
contar� con datos referentes a este dominio.
El juego consiste en que la computadora adivine el nombre de un animal que el usuario est�
pensando para lo cual le har� unas preguntas y basadas en las respuestas tratara de adivinar;
y en caso de que el usuario no haya adivinado, el usuario podr� agregar ese animal que no
adivino.

** Funcionalidades **

* *Carga Arbol*

El programa debe poder leer los datos a partir de un archivo para cual usted cuenta con el
archivo datos.txt que contiene las preguntas y respuestas del dominio de los animales. Los
datos almacenados en este archivo est�n dispuestos en pre-orden.

���

	#P es un mam�fero?
	#R un perro

���

Donde la cadena que empieza con �#P� representa una pregunta (nodo interno) y �#R�
representa que es una respuesta (nodo hoja). Las respuestas siempre son hojas del �rbol.


* *El juego*

Usted deber� mostrar una secuencia de juego gr�fica que le permita al computador adivinar el
animal que el usuario est� pensando, y al mismo tiempo si es que no adivina deber� permitirle
agregar ese animal que no adivino.
A continuaci�n, se muestra un ejemplo por consola, sin embargo, su proyecto debe tener un
entorno gr�fico para la simulaci�n.

���

	Bienvenido a Genio Politecnico!
	Primero...
	Piense en un animal que yo tratar� de adivinarlo...
	Muy bien!	
	Es un mam�fero? Si
	Es un animal dom�stico? Si
	Es un perro!? Si
	He adivinado!
	Quieres jugar de nuevo? Si
	Es un mam�fero? Si
	
	Es un animal dom�stico? No
	Es un animal de granja? No
	Es un elefante!? No
	Boo!
	Ayudame, a mejorar mi predicci�n!
	Que animal estabas pensando? Un tigre
	Escribe una pregunta que me permita diferenciar entre un tigre y un elefante:
	Come carne?
	Para un tigre, la respuesta a la pregunta: �Come carne?�, es si o no?
	Si
	Gracias, he aprendido algo nuevo!
	Quieres jugar de nuevo? No
	Chao!

���

* *Guarda Arbol*

En caso de que el computador no haya adivinado, usted deber� solicitar la respuesta y agregar
una pregunta. Una vez que el usuario haya ingresado estos valores, usted deber� modificar el
�ltimo nodo por donde estuvo el usuario, creando la pregunta y colocando los hijos
dependiendo de la respuesta si es afirmativa o negativa.
Es muy importante que usted tenga la referencia del nodo por la pregunta que est� el usuario,
de lo contrario no podr� almacenar correctamente la nueva informaci�n agregada.



## Ginger Jacome

